package test;
import com.creativemage.tilesheetRenderer.Renderer;
import com.creativemage.tilesheetRenderer.graphicsItem.GraphicsItem;
import motion.Actuate;
import openfl.display.Sprite;

/**
 * ...
 * @author Creative Magic
 */
class TestMain extends Sprite
{
	private var renderer:Renderer;
	private var lib:TestLibrary;

	public function new() 
	{
		super();
		
		setRenderer();
		addSampleAnimation();
		
	}
	
	function setRenderer() 
	{
		lib = new TestLibrary();
		renderer = new Renderer( this.graphics, lib );
		renderer.init();
	}
	
	function addSampleAnimation() 
	{
		var aniBody = new GraphicsItem( TestLibrary.FACE );
		renderer.addChild( aniBody );
		
		Actuate.tween( aniBody, 1, { x:500, y: 200 } ).reflect( true ).repeat( -1 );
	}
	
}