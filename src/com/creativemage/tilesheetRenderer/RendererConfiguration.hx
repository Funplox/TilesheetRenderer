package com.creativemage.tilesheetRenderer;
import openfl.display.Tilesheet;

/**
 * ...
 * @author Creative Magic
 */
class RendererConfiguration
{
	public var framerate:Int = 60;
	public var drawOptions:Int = Tilesheet.TILE_SCALE | Tilesheet.TILE_ROTATION | Tilesheet.TILE_ALPHA;
	
	public var atlasWidth:Int = 2048;
	public var atlasHeight:Int = 2048;

	public function new() 
	{
		
	}
	
}