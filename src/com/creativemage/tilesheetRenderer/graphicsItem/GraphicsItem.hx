package com.creativemage.tilesheetRenderer.graphicsItem;

/**
 * ...
 * @author Creative Magic
 */
@:forward
abstract GraphicsItem(Hidden) 
{
	public function new( str:String )
	{
		this = new Hidden();
	}
}

private class Hidden
{
	public function new()
	{
		
	}
}