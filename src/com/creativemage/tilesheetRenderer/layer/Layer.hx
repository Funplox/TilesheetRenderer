package com.creativemage.tilesheetRenderer.layer;
import com.creativemage.tilesheetRenderer.layer.LayerContent;

/**
 * ...
 * @author Creative Magic
 */
abstract Layer(LayerContent) 
{
	inline function new()
	{
		this = new LayerContent();
	}
	
	@:from
	public static function fromInt(value:Int):Layer
	{
		return new Layer();
	}
	
	
}