package test;
import com.creativemage.tilesheetRenderer.library.ALibrary;

/**
 * ...
 * @author Creative Magic
 */
class TestLibrary extends ALibrary
{
	public static inline var FACE:String = "assets/img/face.png";
	public static inline var BALL_ANIMATION:String = "assets/animations/ball/";

	public function new() 
	{
		super();
	}
	
}