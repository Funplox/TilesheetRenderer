package com.creativemage.tilesheetRenderer;
import com.creativemage.tilesheetRenderer.graphicsItem.GraphicsItem;
import com.creativemage.tilesheetRenderer.layer.Layer;
import com.creativemage.tilesheetRenderer.library.ALibrary;
import openfl.display.BitmapData;
import openfl.display.Graphics;
import openfl.display.Tilesheet;

/**
 * ...
 * @author Creative Magic
 */
class Renderer implements IRenderer
{
	public var atlas(default, null):BitmapData;
	
	private var inited:Bool = false;
	private var tilesheet:Tilesheet;
	
	private var layers:Array<Layer> = []
	private var allItems:Array<GraphicsItem> = [];
	
	private var targetGraphicsObject:Graphics;
	private var graphicsLibrary:ALibrary;

	public function new(graphicsTarget:Graphics, library:ALibrary) 
	{
		
	}
	
	public function init(?config:RendererConfiguration):Void
	{
		if ( inited == true )
			return;
			
		inited = true;
		
		if (config == null)
			config = new RendererConfiguration();
			
		bakeAtlas();
		
		tilesheet = new Tilesheet( atlas );
	}
	
	private function bakeAtlas() 
	{
		atlas = new BitmapData( 1024, 1024, true, 0x00000000 );
	}
	
	public function update():Void
	{
		
	}
	
	/* INTERFACE com.creativemage.tilesheetRenderer.IRenderer */
	
	public function addLayer(layer:Layer):Layer 
	{
		return null;
	}
	
	public function removeLayer(layer:Layer):Void 
	{
		
	}
	
	public function addChild(child:GraphicsItem, ?layer:Layer):GraphicsItem 
	{
		if (layer == null)
			layer = -1;
			
		return null;
	}
	
	public function removeChild(child:GraphicsItem):Void 
	{
		
	}
	
	public function clearLayerContent(layer:Layer):Void 
	{
		
	}
	
	public function clearAll():Void 
	{
		
	}
	
	public function pause():Void 
	{
		
	}
	
	public function resume():Void 
	{
		
	}
	
	public function getLayerAt(index:Int):Layer 
	{
		return null;
	}
	
	public function getAllLayers():Array<Layer> 
	{
		return [];
	}
	
}