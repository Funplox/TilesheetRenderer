package com.creativemage.tilesheetRenderer;
import com.creativemage.tilesheetRenderer.graphicsItem.GraphicsItem;
import com.creativemage.tilesheetRenderer.layer.Layer;

/**
 * @author Creative Magic
 */
interface IRenderer 
{
	public function addLayer(layer:Layer):Layer;
	public function removeLayer(layer:Layer):Void;
	
	public function addChild(child:GraphicsItem, layer:Layer = -1):GraphicsItem;
	public function removeChild(child:GraphicsItem):Void;
	
	public function clearLayerContent(layer:Layer):Void;
	public function clearAll():Void;
	
	public function pause():Void;
	public function resume():Void;
	
	public function getLayerAt(index:Int):Layer;
	public function getAllLayers():Array<Layer>;
	
}